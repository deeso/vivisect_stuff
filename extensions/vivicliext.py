__author__ = 'dso'

import inspect
import imp, sys, time
import vivisect.cli as viv_cli


class VivCliExt(viv_cli.VivCli):

    def do_renameFunction(self, cmd):
        '''
        Rename a function based on its name or virtual address
        The new name must be unique, and the functions VA / name
        should be present if the workspace.

        Usage: renameFunction <va | oldName> newName
            -- newName must be unique!
        '''
        if len(cmd.strip().split()) == 0:
            return self.do_help('renameFunction')

        old_name, new_name = cmd.split()
        va = None
        va = None
        try:
            if old_name.isdigit():
                va = int(old_name)
            else:
                va = int(old_name, 16)
        except:
            func_vas = dict([(self.getName(i), i) for i in self.getFunctions()])
            va = func_vas[old_name] if old_name in func_vas else None
        finally:
            if va is None:
                self.vprint("Function VA could not be found in the workspace.")
                return self.do_help('renameFunction')


        func_vas = dict([(self.getName(i), i) for i in self.getFunctions()])

        if new_name in func_vas:
            self.vprint("Function name has already be used in the workspace.")
            return self.do_help('renameFunction')
        elif not old_name in func_vas:
            self.vprint("Could not find the function name in the workspace.")
            return self.do_help('renameFunction')

        self.makeName(func_vas[old_name], new_name)


    def do_renameLocal(self, cmd):
        '''
        Rename a local variable in a function.  The oldLocalName can be an offset
        into the locals or it can be the name of the variable (if it was already
        named).  The new name can be anything without spaces but it can not be ONLY
        digits.

        Usage: renameFunction <function | va>  <oldLocalName | offset> newLocalName [type]
            -- offsets must already exist in the FunctionLocals Metadata
            -- newName must be unique (locally)!
            -- for my own sanity, new_names are forbidden to be ints!

        '''
        if len(cmd.strip().split()) < 3:
            return self.do_help('renameFunction')

        ltype = None

        args = cmd.strip().split()
        if len(args) == 3:
            function, old_name, new_name = args
        else:
            function, old_name, new_name, ltype = args[:4]

        if new_name.isdigit():
            return self.do_help('renameFunction')

        va = None
        try:
            if function.isdigit():
                va = int(function)
            else:
                va = int(function, 16)
        except:
            func_vas = dict([(self.getName(i), i) for i in self.getFunctions()])
            va = func_vas[function] if function in func_vas else None
        finally:
            if va is None:
                self.vprint("Function VA could not be found in the workspace.")
                return self.do_help('renameLocal')

        # keep in mind locals are ==> (offset, type, lname)
        # map local_vs by their offset
        local_vs = dict([(i[0], i) for i in self.getFunctionLocals(va)])
        # reverse map by names to offset, note, 'local' name will collide!
        names = dict([(local_vs[i][2], i) for i in local_vs ])


        # check the uniqueness of the new_name
        # 0) check that the offset is even present
        # 1) check new local name against the provided offset
        # 2) check new local name against the old_name if it is in names

        if old_name.isdigit() and not int(old_name) in local_vs:
            self.vprint("Local Variable offset is not present in this function.")
            return self.do_help('renameLocal')

        elif new_name in names and \
           old_name.isdigit() and \
           int(old_name) in local_vs and \
           new_name !=  local_vs[int(old_name)][2]:
            self.vprint("Local Variable Name is already used in this scope.")
            return self.do_help('renameLocal')

        elif new_name in names and \
            not old_name.isdigit() and new_name != old_name and \
           new_name != 'local':
            self.vprint("Local Variable Name is already used in this scope.")
            return self.do_help('renameLocal')

        elif new_name == 'local' and\
             old_name == new_name:
            self.vprint("Can't name the variable local.")
            return self.do_help('renameLocal')


        offset = -1
        # handle the case of where offsets are provided
        if old_name.isdigit() and \
           int(old_name) in local_vs:
                offset = int(old_name)

        # the old_name went AWOL, lets give it help
        elif not old_name in names:
            self.vprint("Local var. name could not be found in the workspace.")
            return self.do_help('renameLocal')


        if offset == -1:
            offset = names[old_name]

        # set the ltype from previous version
        if ltype is None:
            ltype = local_vs[offset][1]
        else:
            types_module = "vivisect.impemu.impmagic"

            kls_dict = {}
            for name, obj in inspect.getmembers(sys.modules[types_module]):
                if inspect.isclass(obj):
                    kls_dict[name.lower()] = obj

            if not ltype.lower() in kls_dict:
                self.vprint("Incorrect type specified.")
                self.vprint("Consult vivisect.impemu.impmagic.py for valid types")
                return self.do_help('renameArgument')

            ltype = kls_dict[ltype]


        vw.setFunctionLocal(va, offset, ltype, new_name)



    def do_renameLocation(self, cmd):
        '''
        Rename a particular location in the workspace.
        The location must exist in the workspace, and
        the new name must NOT exist in the name space,
        EXCEPT for when you are changing the old name to
        the new name (wuuhh?!?)

        renameLocation <oldName| va>  newName
            -- newName must be unique!
        '''
        if len(cmd.strip().split()) < 1:
            return self.do_help('renameLocation')


        old_name, new_name = cmd.split()
        va = None
        if old_name.isdigit():
            va = int(va)
        else:
            try:
                va = int(old_name, 16)
            except:
                try:
                    va = self.getLocationByName(old_name)[0]
                except:
                    pass
            finally:
                if va is None:
                    self.vprint("Location could not be found in the workspace.")
                    return self.do_help('renameLocation')

        # An exception gets thrown if the new name does not
        # exist in the workspace
        try:
            new_name_va = self.getLocationByName(new_name)
            if va != new_name_va:
                self.vprint("Location name is already used in the workspace.")
                return self.do_help('renameLocation')
        except:
            pass

        self.makeName(va, new_name)


    def do_renameArgument(self, cmd):
        '''
        Rename the arguments and set the types in a function.
        This can use the function name or it VA. the index
        is which argument to change.  The renaming takes place
        with precedence disabled, otherwise the name is not committed.


        Usage: renameFunction -F <functionName | va> index name [type]
            -F will force the argument add if it exceeds the number of
                function arguments
            -- new name must be unique
        '''

        force_add = False
        function, idx, new_name, atype = None, None, None, None
        args = cmd.strip().split()
        if args[0] == '-F':
            force_add = True
            args = args[1:]

        if len(args) == 4:
            function, idx, new_name, atype = args
        elif len(args) == 3:
            function, idx, new_name = args
        else:
            return self.do_help('renameLocation')

        if not idx.isdigit():
            self.vprint("Index must be an integer in digit format")
            return self.do_help('renameArgument')
        idx = int(idx)

        va = None
        try:
            if function.isdigit():
                va = int(function)
            else:
                va = int(function, 16)
        except:
            func_vas = dict([(self.getName(i), i) for i in self.getFunctions()])
            va = func_vas[function] if function in func_vas else None
        finally:
            if va is None:
                self.vprint("Function VA could not be found in the workspace.")
                return self.do_help('renameArgument')


        argNames = self.getFunctionArgNames(va)
        if new_name in argNames:
            self.vprint("Argument name already used, choose another.")
            return self.do_help('renameArgument')

        if idx >= len(argNames) and not force_add:
            self.vprint("Do you really want to add that argument? it exceeds the # of defined args.")
            return self.do_help('renameArgument')


        if atype is None:
            arg = self.getFunctionArg(va, idx)
            atype = arg[0]
        else:
            types_module = "vivisect.impemu.impmagic"

            kls_dict = {}
            for name, obj in inspect.getmembers(sys.modules[types_module]):
                if inspect.isclass(obj):
                    kls_dict[name.lower()] = obj


            if not atype.lower() in kls_dict:
                self.vprint("Incorrect type specified.")
                self.vprint("Consult vivisect.impemu.impmagic.py for valid types")
                return self.do_help('renameArgument')

            atype = kls_dict[atype]

        self.setFunctionArg(va, idx, atype, aname=new_name, doprec=False)

